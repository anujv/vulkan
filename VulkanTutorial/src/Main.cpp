
#include "Vulkan.h"

int main( int argc, char* argv[] ) {
  
  {
    Vulkan v;

    try {
      v.Run();
    } catch ( const std::exception& e ) {
      std::cout << e.what() << std::endl;
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}
