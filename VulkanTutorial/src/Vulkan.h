#pragma once

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <vector>
#include <string>

#pragma comment(lib, "vulkan-1.lib")
#pragma comment(lib, "glfw3.lib")

#define WIDTH  800
#define HEIGHT 600
#define TITLE  "Vulkan Window"

class Vulkan {
public:
  void Run( void ) {
    InitWindow();
    InitVulkan();
    MainLoop();
    Cleanup();
  }
private:
  void InitWindow( void ) {
    if ( glfwInit() != GLFW_TRUE )
      throw std::runtime_error( "failed to initialize glfw!" );

    glfwWindowHint( GLFW_CLIENT_API, GLFW_NO_API );
    glfwWindowHint( GLFW_RESIZABLE, GLFW_FALSE );

    _window = glfwCreateWindow( WIDTH, HEIGHT, TITLE, nullptr, nullptr );
  }

  void InitVulkan( void ) {
    CreateInstance();
  }

  void MainLoop( void ) {
    while ( !glfwWindowShouldClose( _window ) ) {
      glfwPollEvents();
    }
  }

  void CreateInstance( void ) {
    CheckInstanceExtensions();
    CheckInstanceLayers();

    VkApplicationInfo applicationInfo  = {};
    applicationInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    applicationInfo.pNext              = nullptr;
    applicationInfo.pApplicationName   = "Vulkan Tutorial";
    applicationInfo.applicationVersion = VK_MAKE_VERSION( 0, 0, 1 );
    applicationInfo.pEngineName        = "No Engine";
    applicationInfo.engineVersion      = VK_MAKE_VERSION( 0, 0, 1 );
    applicationInfo.apiVersion         = VK_API_VERSION_1_2;

    VkInstanceCreateInfo instanceCreateInfo    = {};
    instanceCreateInfo.sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.pNext                   = nullptr;
    instanceCreateInfo.flags                   = 0u;
    instanceCreateInfo.pApplicationInfo        = &applicationInfo;
    instanceCreateInfo.enabledLayerCount       = _instance_layers.size();
    instanceCreateInfo.ppEnabledLayerNames     = _instance_layers.data();
    instanceCreateInfo.enabledExtensionCount   = _instance_extensions.size();
    instanceCreateInfo.ppEnabledExtensionNames = _instance_extensions.data();

    if ( vkCreateInstance( &instanceCreateInfo, nullptr, &_instance ) != VK_SUCCESS )
      throw std::runtime_error( "failed to create VkInstance" );
  }

  void CheckInstanceExtensions( void ) {
    uint32_t extensionCount = 0u;
    vkEnumerateInstanceExtensionProperties( nullptr, &extensionCount, nullptr );
    std::vector<VkExtensionProperties> extensions( extensionCount );
    vkEnumerateInstanceExtensionProperties( nullptr, &extensionCount, extensions.data() );

    std::cout << "Available extensions:\n";
    for ( const auto& extension : extensions ) {
      std::cout << "  " << extension.extensionName << "\n";
    }
  }

  void CheckInstanceLayers( void ) {
    uint32_t layerCount = 0u;
    vkEnumerateInstanceLayerProperties( &layerCount, nullptr );
    std::vector<VkLayerProperties> layers( layerCount );
    vkEnumerateInstanceLayerProperties( &layerCount, layers.data() );

    std::cout << "Available layers:\n";
    for ( const auto& layer : layers ) {
      std::cout << "  " << layer.layerName << "\n";
    }
  }

  void Cleanup( void ) {
    vkDestroyInstance( _instance, nullptr );

    glfwDestroyWindow( _window );
    glfwTerminate();
  }
private:
  GLFWwindow* _window    = nullptr;
  VkInstance  _instance  = VK_NULL_HANDLE;

  std::vector<const char*> _instance_layers = {
    "VK_LAYER_KHRONOS_validation"
  };

  std::vector<const char*> _instance_extensions = {
    VK_EXT_DEBUG_UTILS_EXTENSION_NAME
  };
};
